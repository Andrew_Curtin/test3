﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    GameManagerController GameManager;

    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameManagerController.getInstance();
        GameManager.SetUserInterface(GetComponent<UIController>());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TestText1()
    {
        GameObject.Find("Text").GetComponent<Text>().text = "Press 'F' to exit";
    }

    public void TestText2()
    {
        GameObject.Find("Text").GetComponent<Text>().text = "Test Text";
    }
}
