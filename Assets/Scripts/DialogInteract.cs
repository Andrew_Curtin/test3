﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogInteract : MonoBehaviour
{
    public Vector3 dialogPos;
    public bool dialogActive = false;
    //UndeterminedDialogTreeClass dialogTree
    GameManagerController GameManager;

    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameManagerController.getInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if(dialogActive && Input.GetKeyDown(KeyCode.F))
        {
            dialogActive = false;
            GameManager.DeactivateDialog();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKeyDown(KeyCode.Space) && !dialogActive && other.CompareTag("Player")
            /*&& check if other is facing us enough*/)
        {
            dialogActive = true;
            GameManager.Test();
            GameManager.ActivateDialog(this);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(dialogPos+transform.position, 0.5f);
    }
}
