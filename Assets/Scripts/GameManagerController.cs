﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerController
{
    public static GameManagerController instance = null;

    CameraController camera;
    PlayerController player;
    DialogInteract currentDialog;
    UIController userInterface;

    public static GameManagerController getInstance()
    {
        if (instance == null)
            instance = new GameManagerController();

        return instance;
    }

    public void SetPlayer(PlayerController player)
    {
        this.player = player;
        if (this.player != null)
            Debug.Log("player set");
        else
            Debug.Log("Error: player not set");
    }

    public void SetCamera(CameraController camera)
    {
        this.camera = camera;
        if (this.camera != null)
            Debug.Log("camera set");
        else
            Debug.Log("Error: camera not set");
    }

    public void SetUserInterface(UIController userInterface)
    {
        this.userInterface = userInterface;
        if (this.userInterface != null)
            Debug.Log("userInterface set");
        else
            Debug.Log("Error: userInterface not set");
    }

    public void Test()
    {
        Debug.Log("Testing");
    }

    public void ActivateDialog(DialogInteract dialog)
    {
        currentDialog = dialog;

        //Move camera in to position taken from currentDialog
        camera.SwoopIn(currentDialog.dialogPos+currentDialog.transform.position, currentDialog.transform.position);
        
        //stop player movement and fade player model
        player.FadeOut();

        //present dialog options, taken from currentDialog and given to userInterface
        userInterface.TestText1();
    }

    public void DeactivateDialog()
    {
        currentDialog = null;
        
        //clean up ui
        userInterface.TestText2();

        //Move camera back out
        camera.SwoopOut();
        
        //fade player back in and give movement back to player
        player.FadeIn();
    }
}
