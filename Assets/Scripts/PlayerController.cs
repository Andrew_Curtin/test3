﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 6;

    bool canMove;

    Rigidbody rb;
    Renderer my_renderer;
    GameManagerController GameManager;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        my_renderer = GetComponent<Renderer>();
        GameManager = GameManagerController.getInstance();
        GameManager.SetPlayer(GetComponent<PlayerController>());
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            //input
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            //movement
            rb.velocity = (Vector3.right * h + Vector3.forward * v) * speed;

            //facing
            if (h != 0 || v != 0)
                transform.forward = Vector3.right * h + Vector3.forward * v;
        }
    }

    
    /*These should be Coroutines so everything looks smooth*/

    //function to fade out (temporary, not smoothed)
    public void FadeOut()
    {
        if (canMove)
            canMove = false;

        my_renderer.enabled = false;
    }

    //function to fade in (temporary, not smoothed)
    public void FadeIn()
    {
        if (!canMove)
            canMove = true;

        my_renderer.enabled = true;
    }
}
